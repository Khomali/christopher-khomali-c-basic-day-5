﻿using System;
using System.Collections.Generic;

namespace Tugas_Day_5
{
    class Program
    {
        static void Main()
        {
            //nickname
            Console.WriteLine("Welcome to Zuma World!");
            Console.Write("Insert your Nickname : ");
            string nickname = Console.ReadLine();
            Console.WriteLine("Hello," + nickname + " " + "Lets have some fun in Zuma World!");
            Console.WriteLine("------------------------------------------------------------------");

            //weapon
            Console.WriteLine("Before we start our journey, we must to have some weapon.");

            //create a list weapon
            List<weapon> WeaponList = new List<weapon>();

            // add part to list 
            WeaponList.Add(new weapon() { IdWeapon = 1, WeaponName = "Sword", WeaponDamage = 5000 });
            WeaponList.Add(new weapon() { IdWeapon = 2, WeaponName = "Wand", WeaponDamage = 2500 });
            WeaponList.Add(new weapon() { IdWeapon = 3, WeaponName = "Knife", WeaponDamage = 2000 });

            int list = 1;
            foreach (weapon senjata in WeaponList)
            {
                Console.WriteLine(list++ + " " + senjata.WeaponName + " " + senjata.WeaponDamage);
            }

            //equpped weapon
            Rechoose:
            Console.Write("Choose your weapon (Number Please!) : ");
            int ChooseWeapon;
            string value = Console.ReadLine();
            bool hasil = int.TryParse(value, out ChooseWeapon);
            if (hasil == false)
            {
                Console.WriteLine("Please select the numbers has provided (Number Please!)");
                goto Rechoose;
            }

            int darahMonster = 35000;
            int hitCounter = 0;
            foreach (weapon tembak in WeaponList)
          
            {
                if (ChooseWeapon == tembak.IdWeapon)
                {
            
                    Console.WriteLine("\n You had been equip with weapon {0}, damage: {1}\n Monster with 35.000 health point appear",
                        tembak.WeaponName, tembak.WeaponDamage);
                    Console.WriteLine("Hit the Opponent!");

                    while (darahMonster > 0)
                    {
                        darahMonster -= tembak.WeaponDamage;
                        hitCounter++;
                        Console.WriteLine("Monster Health Point = " + darahMonster);
                    }

                    for (int i = 0; i < hitCounter; i++) ;
                    {
                        System.Threading.Thread.Sleep(50 * hitCounter);
                        Console.WriteLine("Monster has defeated, After you hit {0} times.", hitCounter);
                    }
                    Console.WriteLine("You winning the game!");

                    
                   
                }
              
            }
            if (darahMonster > 0)
            {
                Console.WriteLine("Please select the 3 numbers above!");
                goto Rechoose;
            }
        }
    }
}
