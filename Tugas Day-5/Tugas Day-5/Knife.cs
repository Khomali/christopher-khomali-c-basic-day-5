﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tugas_Day_5
{
    public class Knife : weapon
    {
        public Knife()
        {
            IdWeapon = 3;
            WeaponName = "Knife";
            WeaponDamage = 2000;
        }
    }
}
