﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tugas_Day_5
{
    public class Wand : weapon
    {
        public Wand()
        {
            IdWeapon = 2;
            WeaponName = "Wand";
            WeaponDamage = 2500;
        }
    }
}
