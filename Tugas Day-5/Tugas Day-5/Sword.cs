﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tugas_Day_5
{
    public class Sword : weapon
    {
        public Sword()
        {
            IdWeapon = 1;
            WeaponName = "Sword";
            WeaponDamage = 5000;
        }
    }
}
